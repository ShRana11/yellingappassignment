import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.chrome.ChromeDriver;

public class YellingTest {
	
	@Test
	//R1- one person is yelling
	public void OnePersonYelling() {
		String name[] = new String[] {"jenelle"};
		Yelling one = new Yelling(name);
		String test[] = one.Scream();
		System.out.println(test[0]);
		assertEquals("jenelle is yelling", test[0]);
		
	}
	@Test
	//R2- no person is yelling
	public void NoOneYelling() {
		String name[] = new String[] {};
		Yelling one = new Yelling(name);
		String test[] = one.Scream();
		System.out.println(test[0]);
		assertEquals("nobody is yelling", test[0]);
		
	}
	@Test
	//R3- uppercase person is yelling
	public void UpperCaseYelling() {
		String name[] = new String[] {"PETER"};
		Yelling one = new Yelling(name);
		String test[] = one.Scream();
		System.out.println(test[0]);
		assertEquals("PETER is yelling", test[0]);
		
	}
	@Test
	//R4- two persons are yelling
	public void TwoPersonsYelling() {
		String name[] = new String[] {"peter","emad"};
		Yelling one = new Yelling(name);
		String test[] = one.Scream();
		System.out.println(test[0]);
		assertEquals("peter and emad are yelling", test[0]);
		
	}
	@Test
	//R5- more than two persons are yelling
	public void morePersonsYelling() {
		String name[] = new String[] {"peter","emad","jenelle"};
		Yelling one = new Yelling(name);
		String test[] = one.Scream();
		System.out.println(test[0]);
		assertEquals("peter, emad, and jenelle are yelling", test[0]);
		
	}
	@Test
	//R6- two persons and Uppercase is yelling
	public void mixturePersonsYelling() {
		String name[] = new String[] {"peter","JOSH"};
		Yelling one = new Yelling(name);
		String test[] = one.Scream();
		System.out.println(test[0]);
		assertEquals("peter is yelling.SO IT JOSH!", test[0]);
		
	}
	@Test
	//R6- more than two persons and Uppercase are yelling
	public void mixturemorePersonsYelling() {
		String name[] = new String[] {"peter","JOSH","emad","jenelle"};
		Yelling one = new Yelling(name);
		String test[] = one.Scream();
		System.out.println(test[0]);
		assertEquals("peter, emad, and jenelle are yelling.SO IT JOSH!", test[0]);
		
	}

}
